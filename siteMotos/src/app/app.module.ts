import { HomeCarouselComponent } from './components/home/home-carousel/home-carousel.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { StreetComponent } from './components/street/street.component';
import { CustomComponent } from './components/custom/custom.component';
import { TrailComponent } from './components/trail/trail.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { SportComponent } from './components/sport/sport.component';
import { TouringComponent } from './components/touring/touring.component';
import { LoginComponent } from './components/login/login.component';
import { CadastroUsuariosComponent } from './components/cadastro-usuarios/cadastro-usuarios.component';
import { CadastroClientesComponent } from './components/cadastro-clientes/cadastro-clientes.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';
import { AcompanhamentoPedidosComponent } from './components/acompanhamento-pedidos/acompanhamento-pedidos.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import { HomeSlidesComponent } from './components/home/home-slides/home-slides.component';
import { HomeParallaxComponent } from './components/home/home-parallax/home-parallax.component';
import { HomeCardsComponent } from './components/home/home-cards/home-cards.component';
import { FooterComponent } from './components/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    StreetComponent,
    CustomComponent,
    TrailComponent,
    ScooterComponent,
    SportComponent,
    TouringComponent,
    LoginComponent,
    CadastroUsuariosComponent,
    CadastroClientesComponent,
    CadastroMotosComponent,
    AcompanhamentoPedidosComponent,
    NavbarComponent,
    HomeCarouselComponent,
    HomeSlidesComponent,
    HomeParallaxComponent,
    HomeCardsComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IvyCarouselModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
