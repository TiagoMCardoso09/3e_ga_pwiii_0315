import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TouringComponent } from './touring.component';

describe('TouringComponent', () => {
  let component: TouringComponent;
  let fixture: ComponentFixture<TouringComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TouringComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TouringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
