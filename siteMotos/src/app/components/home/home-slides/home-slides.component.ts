import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-slides',
  templateUrl: './home-slides.component.html',
  styleUrls: ['./home-slides.component.css']
})
export class HomeSlidesComponent implements OnInit {

  slides = [
    {
      imagem: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      comprar: 'https://www3.yamaha-motor.com.br/mt-03-abs/product/30021?gclid=CjwKCAjw9LSSBhBsEiwAKtf0ny3DnSn64I-hh0X6lxhD-AdqiHjwzpUwcTPypx_W00jHRAyJBYAWvxoCBoAQAvD_BwE'
    },
    {
      imagem: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      comprar: 'https://www3.yamaha-motor.com.br/mt-03-abs/product/30021?gclid=CjwKCAjw9LSSBhBsEiwAKtf0ny3DnSn64I-hh0X6lxhD-AdqiHjwzpUwcTPypx_W00jHRAyJBYAWvxoCBoAQAvD_BwE'
    },
    {
      imagem: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      comprar: 'https://www3.yamaha-motor.com.br/mt-03-abs/product/30021?gclid=CjwKCAjw9LSSBhBsEiwAKtf0ny3DnSn64I-hh0X6lxhD-AdqiHjwzpUwcTPypx_W00jHRAyJBYAWvxoCBoAQAvD_BwE'
    },
    {
      imagem: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      comprar: 'https://www3.yamaha-motor.com.br/mt-03-abs/product/30021?gclid=CjwKCAjw9LSSBhBsEiwAKtf0ny3DnSn64I-hh0X6lxhD-AdqiHjwzpUwcTPypx_W00jHRAyJBYAWvxoCBoAQAvD_BwE'
    },
    {
      imagem: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      comprar: 'https://www3.yamaha-motor.com.br/mt-03-abs/product/30021?gclid=CjwKCAjw9LSSBhBsEiwAKtf0ny3DnSn64I-hh0X6lxhD-AdqiHjwzpUwcTPypx_W00jHRAyJBYAWvxoCBoAQAvD_BwE'
    },
    {
      imagem: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      comprar: 'https://www3.yamaha-motor.com.br/mt-03-abs/product/30021?gclid=CjwKCAjw9LSSBhBsEiwAKtf0ny3DnSn64I-hh0X6lxhD-AdqiHjwzpUwcTPypx_W00jHRAyJBYAWvxoCBoAQAvD_BwE'
    },
    {
      imagem: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      comprar: 'https://www3.yamaha-motor.com.br/mt-03-abs/product/30021?gclid=CjwKCAjw9LSSBhBsEiwAKtf0ny3DnSn64I-hh0X6lxhD-AdqiHjwzpUwcTPypx_W00jHRAyJBYAWvxoCBoAQAvD_BwE'
    },
    {
      imagem: 'https://dd5394a0b8ca8e97ba29-abf76f3d91a2125517d6c7c409f095c7.ssl.cf1.rackcdn.com/content/common/Models/2022/64925165-3c41-4119-b203-3c28b4847527.png',
      comprar: 'https://www3.yamaha-motor.com.br/mt-03-abs/product/30021?gclid=CjwKCAjw9LSSBhBsEiwAKtf0ny3DnSn64I-hh0X6lxhD-AdqiHjwzpUwcTPypx_W00jHRAyJBYAWvxoCBoAQAvD_BwE'
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
