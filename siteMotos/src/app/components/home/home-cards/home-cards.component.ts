import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-cards',
  templateUrl: './home-cards.component.html',
  styleUrls: ['./home-cards.component.css']
})
export class HomeCardsComponent implements OnInit {

  cards = [
    {
      imagem: 'https://www.mototur.pt/wp-content/uploads/2018/12/NC-750-Vermelho.png',
      titulo: 'NC 750X ',
      valor: '55.700',
      saibamais: ''
    },
    {
      imagem: 'https://www.mototur.pt/wp-content/uploads/2018/12/NC-750-Vermelho.png',
      titulo: 'NC 750X ',
      valor: '55.700',
      saibamais: ''
    },
    {
      imagem: 'https://www.mototur.pt/wp-content/uploads/2018/12/NC-750-Vermelho.png',
      titulo: 'NC 750X ',
      valor: '55.700',
      saibamais: ''
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
