import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-carousel',
  templateUrl: './home-carousel.component.html',
  styleUrls: ['./home-carousel.component.css']
})
export class HomeCarouselComponent implements OnInit {

  classeSumir = 'carousel-indicators'

  carousel = [
    {
      imagem: 'https://content2.kawasaki.com/ContentStorage/KMB/HomePagePromo/18/d654def7-a062-46fe-bdf6-0835c1007a48.jpg?w=1920',
      titulo: 'Nossa rede de concessionárias está online para você.',
      subtitulo: 'Com o localizador fica fácil encontrar a Kawasaki mais perto de você.',
      active: 'active'
    },
    {
      imagem: 'https://www3.yamaha-motor.com.br/file/general/yma-home-racing-banner.png',
      titulo: 'YAMAHA RACING BRASIL',
      subtitulo: 'A competição faz parte do nosso DNA'
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

  sumir() {
    this.classeSumir = 'd-none'
  }

  aparecer() {
    this.classeSumir = 'carousel-indicators'
  }

}
