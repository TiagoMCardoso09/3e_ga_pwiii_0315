import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeCarouselComponent } from './home-carousel/home-carousel.component';
import { HomeSlidesComponent } from './home-slides/home-slides.component';
import { HomeParallaxComponent } from './home-parallax/home-parallax.component';
import {IvyCarouselModule} from 'angular-responsive-carousel';
import { HomeCardsComponent } from './home-cards/home-cards.component';


@NgModule({
  declarations: [
    HomeCarouselComponent,
    HomeSlidesComponent,
    HomeParallaxComponent,
    HomeCardsComponent
  ],
  imports: [
    CommonModule,
    IvyCarouselModule
  ],
  exports: [
    HomeCarouselComponent,
    HomeSlidesComponent,
    HomeCarouselComponent
  ]
})
export class HomeModule { }
