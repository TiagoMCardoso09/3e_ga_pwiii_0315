import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  click = false
  classeRotacao = 'navbar-toggler'

  constructor() { }

  ngOnInit(): void {
  }

  rotacao() {
    this.click = !this.click
    if (this.click == true) {
      this.classeRotacao = 'navbar-toggler rotacao'
    } else {
      this.classeRotacao = 'navbar-toggler rotacao-voltar'
    }
  }

}
